var gulp = require('gulp'),
    glp  = require('gulp-load-plugins')();

gulp.task('sass', function() {
    gulp.src('khrapin_core/cartridge/scss/default/style.scss')
        .pipe(glp.sourcemaps.init())
        .pipe(glp.sass())
        .pipe(glp.autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
        .pipe(glp.sourcemaps.write())
        .pipe(gulp.dest('khrapin_core/cartridge/static/default/css/'));
});

gulp.task('watch', ['sass'], function() {
    gulp.watch('khrapin_core/cartridge/scss/default/**/*', ['sass']);
    // gulp.watch('khrapin_core/**/*', ['upload']);
});

// autoloading cartridge to DW server (version 1 - using plugin gulp-upload) - start
var options = {
    server: 'http://astound02.alliance-prtnr-eu01.dw.demandware.net',
    data: {
        dirname: ['khrapin_core', 'khrapin_controllers'],
        fileName: 'dw.json'
    },
    callback: function (err, data, res) {
        if (err) {
            console.log('error:' + err.toString());
        } else {
            console.log(data.toString());
        }
    }
};

gulp.task('upload', function() {
    return gulp.src(['khrapin_core', 'khrapin_controllers'])
        .pipe(glp.upload(options));
});
// autoloading cartridge to DW server - end

// autoloading cartridge to DW server (version 2 - using plugin dwdav) - start
// var dwdav = require('dwdav');
// var path = require('path');
// var config = require('@tridnguyen/config');
// var gutil = require('gulp-util');
//
// function upload(files) {
//     var credentials = config('dw.json', {caller: false});
//     var server = dwdav(credentials);
//     Promise.all(files.map(function (f) {
//         return server.post(path.relative(process.cwd(), f));
//     })).then(function() {
//         gutil.log(gutil.colors.green('Uploaded ' + files.join(',') + ' to the server'));
//     }).catch(function(err) {
//         gutil.log(gutil.colors.red('Error uploading ' + files.join(','), err));
//     });
// }
//
// gulp.task('watch:server', function() {
//     gulp.watch(['khrapin_core/**/*'], {}, function(event) {
//             upload([event.path]);
//         }
//     );
// });
// autoloading cartridge to DW server - end




gulp.task('default', ['watch']);